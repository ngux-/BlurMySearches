# Blur My Searches

## Multiply your web searches to add irrelevant data to what is collected about you

### This project has not been started yet.
### This README is, for now, mainly here to guide the project, and will be tidied up later.

Totally untraced web browsing isn't possible. There are a lot of tools out there
to make things better, but being totally anonymous is not possible, and having no
data being collected is also not possible... unless you live in a cave, but then
you wouldn't be reading this anyway.

I'm sure this tool already exists in some form, but here's my version.

## Usage

The idea would be running the program in the background while browsing. It
would gather anything that is searched and would *simultaneously* issue tens of
other random searches.
The randomness of these other searches could be fined tuned with dictionnaries of
words so that the other searches would somewhat resemble the effective search.

For example I'm buying shoes online, I type in 'boots', the program simultaneously
searches the same website for 'sandals', 'santiags', 'socks', 'runners'... you get
the idea.

## Implementations expected

The background program needs to know a few things:
- When to issue a search (typically the user could just run the program manually
when he/she wants to browse, but it would be better to issue searches at the precise
same moment the user sends his/hers).
- What the user types: keylogger? Or catch HTTP? What about HTTPS? Or read the
GET request.
- Obviously, which website the user is browsing.
