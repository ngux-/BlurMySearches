#!/bin/bash

# This file runs firefox in a state where the HTTP requests will be logged to the specified file.

firefox -MOZ_LOG=timestamp,rotate:200,nsHttp:3,sync -MOZ_LOG_FILE=./requests/log.txt
cat ./requests/log.txt.* | grep GET | grep q=
cat ./requests/log.txt.* | grep GET | grep query=
cat ./requests/log.txt.* | grep GET | grep querry=
cat ./requests/log.txt.* | grep GET | grep search=
